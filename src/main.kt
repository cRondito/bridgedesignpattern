fun main(args: Array<String>)
{
    val enchantedSword = Sword(SoulEatingEnchantment())
    enchantedSword.wield()
    println()
    enchantedSword.swing()
    println()
    //enchantedSword.unwield()

    val hammer = Hammer(FlyingEnchantment())
    hammer.wield()
    println()
    hammer.swing()
    println()
    //hammer.unwield()

}