interface Weapon {
    val enchantment: Enchantment
    fun wield()
    fun swing()
    fun unwield()
}

class Sword(override val enchantment: Enchantment) : Weapon {

    override fun wield() {
        println("The sword is wielded")
        enchantment.onActivate()
    }

    override fun swing() {
        println("The sword is swinged")
        enchantment.apply()
    }

    override fun unwield() {
        println("The sword is unwielded")
        enchantment.onDeactivate()
    }
}

class Hammer(override val enchantment: Enchantment) : Weapon {

    override fun wield() {
        println("The hammer is wielded")
        enchantment.onActivate()
    }

    override fun swing() {
        println("The hammer is swinged")
        enchantment.apply()
    }

    override fun unwield() {
        println("The hammer is unwielded")
        enchantment.onDeactivate()
    }
}