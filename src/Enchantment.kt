interface Enchantment {
    fun onActivate()
    fun apply()
    fun onDeactivate()
}

class FlyingEnchantment : Enchantment {

    override fun onActivate() {
        println("The item begins to glow faintly")
    }

    override fun apply() {
        println("The item flies and strikes the enemies finally returning to owner's hand")
    }

    override fun onDeactivate() {
        println("The item's glow fades")
    }
}

class SoulEatingEnchantment : Enchantment {

    override fun onActivate() {
        println("The item spreads bloodlust")
    }

    override fun apply() {
        println("The item eats the soul of enemies")
    }

    override fun onDeactivate() {
        println("Bloodlust slowly disappears")
    }
}